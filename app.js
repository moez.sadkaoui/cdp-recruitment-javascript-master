const { getArg, getCommand } = require('./lib/cli');
const { filterReducer, counterReducer } = require('./lib');
const { data } = require('./data');

const [args] = process.argv.slice(2);
const command = getCommand(args);
const arg = getArg(args);

const init = (data) => {

    switch (command) {
        case 'filter':
            
            if (!arg) {
                console.log(data);
                break;
            }
            let filtredData = data.reduce(filterReducer(arg), []);
            
            console.log(JSON.stringify(filtredData,null, 2));
            break;
        case 'count':
            
            let countedData = data.reduce(counterReducer, []);
            console.log(JSON.stringify(countedData, null, 2));
            break;
        default:
            console.error('invalid args');
            break;
    }
}

init(data);

