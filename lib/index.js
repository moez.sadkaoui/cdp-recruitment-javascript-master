const { filterReducer } = require('./filter');
const { counterReducer } = require('./counter');

module.exports = {
  filterReducer,
  counterReducer,
}