const { filterReducer, counterReducer } = require('../../lib');
const { data } = require('../../data');

function reduceFilterToTest(arg){
    let filtredData = data.reduce(filterReducer(arg), [])
    return JSON.stringify(filtredData,null, 2)
}
function reduceCounterToTest(){
    let countedData = data.reduce(counterReducer, []);
           return JSON.stringify(countedData, null, 2);
}

module.exports = {
    reduceFilterToTest,reduceCounterToTest}