const { reduceFilterToTest } = require('./functionToBeTested')
var array = [
    {
        name: 'Uzuzozne',
        people: [
          {
            name: 'Lillie Abbott',
            animals: [
              {
                name: 'John Dory'
              }
            ]
          }
        ]
      },
      {
        name: 'Satanwi',
        people: [
          {
            name: 'Anthony Bruno',
            animals: [
              {
                name: 'Oryx'
              }
            ]
          }
        ]
      }
  ]
var result = JSON.stringify(array,null, 2)
test('filter test',()=> {
    expect(reduceFilterToTest("ry")).toContain(result)
})