const valideName = (animal, criteria) => {
  return animal && animal.name && animal.name.toLowerCase().includes(criteria);
}

const filterAnimals = (animals, criteria) => {
  return animals.filter(animal => {
      return valideName(animal, criteria);
  });
}

function filterReducer(arg) {
  return reducer = (filtredData, contry) => {
    const arrayData = {}
    arrayData.name = contry.name;

    contry.people.forEach((people) => {
        arrayData.people = arrayData.people || [];
        const arrayPeople = {
            name: people.name,
            animals: filterAnimals(people.animals, arg)
        }

        if (arrayPeople.animals.length)
            arrayData.people.push(arrayPeople)
    });

    if (arrayData.people.length) {
        filtredData.push(arrayData);
    }

    return filtredData;
  };
};

module.exports = {
  filterReducer
}